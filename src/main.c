#include "mem.h"
#include "util.h"
#include <assert.h>
#include <stdio.h>

void initialize_heap(size_t size) {
    heap_init(size);
    printf("=== Initialized heap with size: %zu ===\n", size);
    debug_heap(stdout, HEAP_START);
}

void finalize_heap() {
    debug_heap(stdout, HEAP_START);
    heap_term();
    printf("=== Heap finalized ===\n\n");
}

void test_basic_allocation_scenario() {
    initialize_heap(4096);
    printf("-- Test: Basic allocation --\n");
    void* mem = _malloc(100);
    assert(mem != NULL && "Allocation failed");
    debug_heap(stdout, HEAP_START);
    _free(mem);
    finalize_heap();
}

void test_freeing_single_scenario() {
    initialize_heap(4096);
    printf("-- Test: Freeing single block --\n");
    void* mem = _malloc(50);
    assert(mem != NULL && "Allocation failed");
    debug_heap(stdout, HEAP_START);
    _free(mem);
    finalize_heap();
}

void test_multiple_blocks_free_scenario() {
    initialize_heap(4096);
    printf("-- Test: Freeing multiple blocks --\n");
    void* mem1 = _malloc(50);
    void* mem2 = _malloc(50);
    assert(mem1 != NULL && "Allocation failed for first block");
    assert(mem2 != NULL && "Allocation failed for second block");
    debug_heap(stdout, HEAP_START);
    _free(mem1);
    _free(mem2);
    finalize_heap();
}

void test_heap_extension_scenario() {
    initialize_heap(4096);
    printf("-- Test: Heap extension --\n");
    void* mem1 = _malloc(3000);
    void* mem2 = _malloc(4000);
    assert(mem1 != NULL && "First allocation failed");
    assert(mem2 != NULL && "Second allocation failed");
    debug_heap(stdout, HEAP_START);
    _free(mem1);
    _free(mem2);
    finalize_heap();
}

void test_outer_allocation() {
    initialize_heap(4096);
    printf("-- Test: Allocation in another place --\n");
    void* obstacle = _malloc(4096);
    void* mem = _malloc(5000);
    assert(obstacle != NULL && "Another place allocation failed");
    assert(mem != NULL && "Allocation failed");
    debug_heap(stdout, HEAP_START);
    _free(obstacle);
    _free(mem);
    finalize_heap();
}

void test_heap_min_size(size_t first_alloc_size) {
    assert(first_alloc_size < 24 && "First allocation size must be less than 24 bytes");

    initialize_heap(512);
    printf("-- Test: Heap Minimum Size with first allocation of %zu bytes --\n", first_alloc_size);

    int *mem1 = _malloc(first_alloc_size);
    assert(mem1 != NULL && "Allocation failed");
    debug_heap(stdout, HEAP_START);

    _free(mem1);

    int *mem2 = _malloc(24);
    assert(mem2 != NULL && "Second allocation failed");
    debug_heap(stdout, HEAP_START);

    printf("Address 1: %p, Address 2: %p\n", (void*)mem1, (void*)mem2);
    assert((uintptr_t)mem1 == (uintptr_t)mem2 && "Addresses do not match for allocations of different sizes");

    printf("Test passed: Heap minimum size\n");

    _free(mem2);
    finalize_heap();
}

int main() {
    test_basic_allocation_scenario();
    test_freeing_single_scenario();
    test_multiple_blocks_free_scenario();
    test_heap_extension_scenario();
    test_outer_allocation();
    
    for (size_t size = 1; size < 24; ++size) {
        test_heap_min_size(size);
    }
    
    return 0;
}
