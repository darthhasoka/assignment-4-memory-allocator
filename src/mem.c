#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}


/*  Р°Р»Р»РѕС†РёСЂРѕРІР°С‚СЊ СЂРµРіРёРѕРЅ РїР°РјСЏС‚Рё Рё РёРЅРёС†РёР°Р»РёР·РёСЂРѕРІР°С‚СЊ РµРіРѕ Р±Р»РѕРєРѕРј */
static struct region alloc_region(void const *addr, size_t capacity) {
    size_t size_needed = region_actual_size(size_from_capacity((block_capacity){.bytes = capacity}).bytes);
    void *alloc_addr = map_pages(addr, size_needed, MAP_FIXED);

    if (alloc_addr == MAP_FAILED) {
        alloc_addr = map_pages(addr, size_needed, 0);
        if (alloc_addr == MAP_FAILED) {
            return REGION_INVALID;
        }
    }

    struct block_header *header = alloc_addr;
    block_init(header, (block_size){.bytes = size_needed}, NULL);
    struct region region = {.addr = alloc_addr, .size = size_needed, .extends = alloc_addr == addr};
    return region;
}

static bool blocks_continuous (
        struct block_header const* fst,
        struct block_header const* snd );

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/*  РѕСЃРІРѕР±РѕРґРёС‚СЊ РІСЃСЋ РїР°РјСЏС‚СЊ, РІС‹РґРµР»РµРЅРЅСѓСЋ РїРѕРґ РєСѓС‡Сѓ */
void heap_term() {
    struct block_header* header = HEAP_START;
    while(header != NULL){
        size_t region_size = 0;
        void* region_start = header;
        while(header -> next == block_after(header)){
            region_size += size_from_capacity(header -> capacity).bytes;
            header = header -> next;
        }
        region_size += size_from_capacity(header -> capacity).bytes;
        header = header -> next;
        assert(munmap(region_start, region_size) != -1);
    }
}


#define BLOCK_MIN_CAPACITY 24

/*  --- Р Р°Р·РґРµР»РµРЅРёРµ Р±Р»РѕРєРѕРІ (РµСЃР»Рё РЅР°Р№РґРµРЅРЅС‹Р№ СЃРІРѕР±РѕРґРЅС‹Р№ Р±Р»РѕРє СЃР»РёС€РєРѕРј Р±РѕР»СЊС€РѕР№ )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}




static bool split_if_too_big(struct block_header *header, size_t query)
{
    if (header && block_splittable(header, query)) {
        block_size first_part = size_from_capacity((block_capacity){query});
        block_size second_part = (block_size){size_from_capacity(header->capacity).bytes - first_part.bytes};

        void* second_block = (uint8_t*)header + first_part.bytes;

        block_init(second_block, second_part, header->next);
        block_init(header, first_part, second_block);
        return true;
    }
    return false;
}


/*  --- РЎР»РёСЏРЅРёРµ СЃРѕСЃРµРґРЅРёС… СЃРІРѕР±РѕРґРЅС‹С… Р±Р»РѕРєРѕРІ --- */

static void* block_after( struct block_header const* block ) {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}


static bool try_merge_with_next(struct block_header* block) {
    if (!block->next || !block->is_free || !block->next->is_free || !mergeable(block, block->next)) {
        return false;
    }
    struct block_header *next_header = block -> next;
    block -> capacity = (block_capacity) {.bytes = block -> capacity.bytes + size_from_capacity(next_header -> capacity).bytes};
    block -> next = next_header -> next;
    return true;
}


/*  --- ... ecР»Рё СЂР°Р·РјРµСЂР° РєСѓС‡Рё С…РІР°С‚Р°РµС‚ --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};



static struct block_search_result find_good_or_last(struct block_header *header, size_t sz) {
    struct block_header *last = NULL;
    for (struct block_header *current = header; current; current = current->next) {
        while (try_merge_with_next(current));
        if (current->is_free && block_is_big_enough(sz, current)) {
            return (struct block_search_result){.type = BSR_FOUND_GOOD_BLOCK, .block = current};
        }
        last = current;
    }
    return (struct block_search_result){.type = BSR_REACHED_END_NOT_FOUND, .block = last};
}


/*  РџРѕРїСЂРѕР±РѕРІР°С‚СЊ РІС‹РґРµР»РёС‚СЊ РїР°РјСЏС‚СЊ РІ РєСѓС‡Рµ РЅР°С‡РёРЅР°СЏ СЃ Р±Р»РѕРєР° `block` РЅРµ РїС‹С‚Р°СЏСЃСЊ СЂР°СЃС€РёСЂРёС‚СЊ РєСѓС‡Сѓ
 РњРѕР¶РЅРѕ РїРµСЂРµРёСЃРїРѕР»СЊР·РѕРІР°С‚СЊ РєР°Рє С‚РѕР»СЊРєРѕ РєСѓС‡Сѓ СЂР°СЃС€РёСЂРёР»Рё. */

static struct block_search_result try_memalloc_existing(size_t query, struct block_header *header) {
    struct block_search_result result = find_good_or_last(header, query);
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        if (split_if_too_big(result.block, query)) {
            result.block->is_free = false;
        }
    }
    return result;
}




static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    if (last == NULL) return NULL;

    struct region next_region = alloc_region(block_after(last), query);
    if (region_is_invalid(&next_region)) return NULL;

    last->next = next_region.addr;

    if (next_region.extends && try_merge_with_next(last)) {
        return last;
    }

    return last->next;
}


/*  Р РµР°Р»РёР·СѓРµС‚ РѕСЃРЅРѕРІРЅСѓСЋ Р»РѕРіРёРєСѓ malloc Рё РІРѕР·РІСЂР°С‰Р°РµС‚ Р·Р°РіРѕР»РѕРІРѕРє РІС‹РґРµР»РµРЅРЅРѕРіРѕ Р±Р»РѕРєР° */
static struct block_header* memalloc(size_t request_size, struct block_header* heap_start) {
    size_t size_needed = (request_size >= BLOCK_MIN_CAPACITY) ? request_size : BLOCK_MIN_CAPACITY;

    struct block_search_result search_result = try_memalloc_existing(size_needed, heap_start);
    if (search_result.type == BSR_FOUND_GOOD_BLOCK) {
        return search_result.block;
    }

    struct block_header* new_block = (search_result.type != BSR_CORRUPTED) ? grow_heap(search_result.block, size_needed) : NULL;
    if (!new_block) {
        return NULL;
    }

    search_result = try_memalloc_existing(size_needed, new_block);
    if (search_result.type == BSR_FOUND_GOOD_BLOCK) {
        return search_result.block;
    }

    return NULL;
}



void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  try_merge_with_next(header);
}
